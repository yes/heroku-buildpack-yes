YES build pack
========================

Pre-compiling binaries
----------------------

    #!/bin/bash
    set -uex
    cd /tmp

    # Heroku revision.  Must match in 'compile' program.
    #
    # Affixed to all vendored binary output to represent changes to the
    # compilation environment without a change to the upstream version,
    # e.g. PHP 5.3.27 without, and then subsequently with, libmcrypt.
    heroku_rev='-2'

    # Clear /app directory
    find /app -mindepth 1 -print0 | xargs -0 rm -rf

    # Take care of vendoring libmcrypt
    mcrypt_version=2.5.8
    mcrypt_dirname=libmcrypt-$mcrypt_version
    mcrypt_archive_name=$mcrypt_dirname.tar.bz2

    # Download mcrypt if necessary
    if [ ! -f mcrypt_archive_name ]
    then
        curl -Lo $mcrypt_archive_name http://sourceforge.net/projects/mcrypt/files/Libmcrypt/$mcrypt_version/$mcrypt_archive_name/download
    fi

    # Clean and extract mcrypt
    rm -rf $mcrypt_dirname
    tar jxf $mcrypt_archive_name

    # Build and install mcrypt.
    pushd $mcrypt_dirname
    ./configure --prefix=/app/vendor/mcrypt \
      --disable-posix-threads --enable-dynamic-loading
    make -s
    make install -s
    popd

    # Take care of vendoring ImageMagick
    imagemagick_version=6.8.8-4
    imagemagick_dirname=ImageMagick-$imagemagick_version
    imagemagick_archive_name=$imagemagick_dirname.tar.bz2

    # Download ImageMagick if necessary
    if [ ! -f imagemagick_archive_name ]
    then
        curl -Lo $imagemagick_archive_name http://mirrors.linsrv.net/ImageMagick/$imagemagick_archive_name
    fi

    # Clean and extract ImageMagick
    rm -rf $imagemagick_dirname
    tar jxf $imagemagick_archive_name

    # Build and install ImageMagick.
    pushd $imagemagick_dirname
    ./configure --prefix=/app/imagemagick
    make -s
    make install -s
    popd

    # Take care of vendoring Apache.
    httpd_version=2.2.26
    httpd_dirname=httpd-$httpd_version
    httpd_archive_name=$httpd_dirname.tar.bz2

    # Download Apache if necessary.
    if [ ! -f $httpd_archive_name ]
    then
        curl -LO ftp://ftp.osuosl.org/pub/apache//httpd/$httpd_archive_name
    fi

    # Clean and extract Apache.
    rm -rf $httpd_dirname
    tar jxf $httpd_archive_name

    # Build and install Apache.
    pushd $httpd_dirname
    ./configure --prefix=/app/apache --enable-rewrite --enable-so --enable-deflate --enable-expires --enable-headers --with-included-apr --enable-proxy --enable-proxy-http
    make -s
    make install -s
    popd

    # Take care of vendoring PHP.
    php_version=5.5.8
    php_dirname=php-$php_version
    php_archive_name=$php_dirname.tar.bz2

    # Download PHP if necessary.
    if [ ! -f $php_archive_name ]
    then
        curl -Lo $php_archive_name http://us1.php.net/get/$php_archive_name/from/www.php.net/mirror
    fi

    # Clean and extract PHP.
    rm -rf $php_dirname
    tar jxf $php_archive_name

    # Compile PHP
    pushd $php_dirname
    ./configure --prefix=/app/php --with-apxs2=/app/apache/bin/apxs                     \
    --with-mysql --with-mysqli --with-pdo-mysql --with-pgsql --with-pdo-pgsql           \
    --with-iconv --with-gd --with-curl=/usr/lib                                         \
    --with-config-file-path=/app/php --enable-soap=shared                               \
    --with-openssl --with-mcrypt=/app/vendor/mcrypt --enable-sockets                    \
    --enable-mbstring --enable-mbregex
    make -s
    make install -s
    popd

    # Copy in MySQL client library.
    mkdir -p /app/php/lib/php
    cp /usr/lib/libmysqlclient.so.16 /app/php/lib/php

    # 'apc' installation
    #
    # $PATH manipulation Necessary for 'pecl install', which relies on
    # PHP binaries relative to $PATH.

    export PATH=/app/imagemagick/bin:/app/php/bin:$PATH
    export LD_LIBRARY_PATH=/app/imagemagick/lib:/app/php/lib/php${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}

    /app/php/bin/pecl channel-update pecl.php.net

    # Use defaults for apc build prompts.
    yes '' | /app/php/bin/pecl install APCu-beta

    # Imagick installation
    imagick_version=3.1.2
    curl -O http://pecl.php.net/get/imagick-$imagick_version.tgz
    tar -zxf imagick-$imagick_version.tgz
    cd imagick-$imagick_version
    phpize
    ./configure --with-imagick=/app/imagemagick
    make
    make install
    cd ../

    # Sanitize default cgi-bin to rid oneself of Apache sample
    # programs.
    find /app/apache/cgi-bin/ -mindepth 1 -print0 | xargs -0 rm -r

    # Stamp and archive binaries.
    pushd /app
    echo $mcrypt_version > vendor/mcrypt/VERSION
    tar -zcf mcrypt-"$mcrypt_version""$heroku_rev".tar.gz vendor/mcrypt
    echo $imagemagick_version > imagemagick/VERSION
    tar -zcf imagemagick-"$imagemagick_version""$heroku_rev".tar.gz imagemagick
    echo $httpd_version > apache/VERSION
    tar -zcf apache-"$httpd_version""$heroku_rev".tar.gz apache
    echo $php_version > php/VERSION
    tar -zcf php-"$php_version""$heroku_rev".tar.gz php
    popd


    echo "scp -r /app/mcrypt-""$mcrypt_version""$heroku_rev"".tar.gz user@domain:/folder"
    echo "scp -r /app/imagemagick-""$imagemagick_version""$heroku_rev"".tar.gz user@domain:/folder"
    echo "scp -r /app/apache-""$httpd_version""$heroku_rev"".tar.gz user@domain:/folder"
    echo "scp -r /app/php-""$php_version""$heroku_rev"".tar.gz user@domain:/folder"