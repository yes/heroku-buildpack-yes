export PATH=/app/imagemagick/bin:/app/php/bin:/usr/local/bin:/usr/bin:/bin
export LD_LIBRARY_PATH=/app/imagemagick/lib:/app/php/lib/php
export MAGICK_HOME=/app/imagemagick
export LANG=en_US.UTF-8